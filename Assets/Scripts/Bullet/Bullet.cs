﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private BulletsProxy _bulletsProxy;
    private Coroutine _lifeTime;
    private WaitForSeconds _lifeTimeWaitSecond;
    private Vector3 _direction;
    private float _speed;
    
    public Vector3 Direction
    {
        private get => _direction;
        set
        {
            var newDirection = value;
            newDirection.z = 0;
            _direction = newDirection;
        }
        
    }

    private void Awake()
    {
        _bulletsProxy = BulletsProxy.Instance;
    }

    private void Start()
    {
        _lifeTimeWaitSecond = new WaitForSeconds(_bulletsProxy.Data.LifeTime);
        _lifeTime = StartCoroutine(LifeTime());
        _speed = _bulletsProxy.Data.Speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_lifeTime != null)
            StopCoroutine(_lifeTime);
        
        _bulletsProxy.ReturnBullet(this);
    }

    private void Update()
    {
        transform.Translate(Direction * Time.deltaTime * _speed);
    }

    private IEnumerator LifeTime()
    {
        yield return _lifeTimeWaitSecond;
        _bulletsProxy.ReturnBullet(this);
    }
}
