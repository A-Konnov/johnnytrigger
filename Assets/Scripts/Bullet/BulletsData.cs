﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BulletsData", menuName = "BulletsData")]
public class BulletsData : ScriptableObject
{
    [Header("Prefabs")]
    [SerializeField] private GameObject _prefab;
    
    [Header("General")]
    [SerializeField] private int _startCount = 6;
    [SerializeField] private float _lifeTime = 5f;
    [SerializeField] private float _speed = 10f;
    
    [Header("UI")]
    [SerializeField] private GameObject _prefabUI;
    [SerializeField] private Color _colorAvailable = Color.yellow;
    [SerializeField] private Color _colorNotAvailable = Color.black;

    public GameObject Prefab => _prefab;
    public GameObject PrefabUI => _prefabUI;
    public int StartCount => _startCount;
    public float LifeTime => _lifeTime;
    public float Speed => _speed;

    public Color ColorAvailable => _colorAvailable;

    public Color ColorNotAvailable => _colorNotAvailable;
}
