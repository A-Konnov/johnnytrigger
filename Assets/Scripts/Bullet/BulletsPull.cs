﻿using System.Collections.Generic;
using UnityEngine;

public class BulletsPull
{
    private readonly GameObject _prefab;
    private Transform _collector;
    private Stack<Bullet> _bullets;
    
    public BulletsPull(GameObject prefab)
    {
        _collector = new GameObject().transform;
        _collector.name = "BulletsPull";
        _prefab = prefab;
        _bullets = new Stack<Bullet>();
    }

    public Bullet GetBullet()
    {
        var bullet = _bullets.Count <= 0 ? Object.Instantiate(_prefab, _collector).GetComponent<Bullet>() : _bullets.Pop();
        bullet.gameObject.SetActive(true);
        return bullet;
    }

    public void ReturnBullet(Bullet bullet)
    {
        bullet.gameObject.SetActive(false);
        _bullets.Push(bullet);
    }
}
