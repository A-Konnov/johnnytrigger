﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletsProxy : Proxy<BulletsProxy, BulletsData>
{
    private BulletsPull _bulletsPull;
    public Action<int> OnGetBullet;
    public int BulletsCount { get; set; }

    public Bullet GetBullet()
    {
        if (BulletsCount <= 0)
            return null;

        BulletsCount--;
        OnGetBullet?.Invoke(BulletsCount);
        return _bulletsPull.GetBullet();
    }

    public void ReturnBullet(Bullet bullet)
    {
        _bulletsPull.ReturnBullet(bullet);
    }

    public override void GameAwake()
    {
        _bulletsPull = new BulletsPull(Data.Prefab);
    }

    public override void GameStart()
    {
        BulletsCount = Data.StartCount;
    }

    public override void GameEnd()
    {
        OnGetBullet = null;
    }
}
