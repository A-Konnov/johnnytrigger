﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider))]
public abstract class APoint : MonoBehaviour
{
    protected const float MinVisitedDistance = 0.1f;
    private PathManager _pathManager;
    private BoxCollider _collider;
  
    protected bool IsVisited { get; set; }
    protected PathManager PathManager => _pathManager;
    
    public int Index { get; set; }
    
    private void Awake()
    {
        _pathManager = PathManager.Instance;
        _collider = GetComponent<BoxCollider>();
    }

    private void Start()
    {
        _collider.isTrigger = true;
    }

    private void OnTriggerStay(Collider other)
    {
        if (IsVisited)
            return;
        
        if (!other.CompareTag("Player"))
            return;
        
        var distance = Math.Abs(other.transform.position.x - transform.position.x);
        if (distance <= MinVisitedDistance)
        {
            IsVisited = true;

            ActivateVisit();
        }
    }

    protected abstract void ActivateVisit();
}
