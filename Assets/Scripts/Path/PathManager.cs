﻿using System.Collections;
using System.Collections.Generic;
using PathCreation;
using UnityEngine;

[RequireComponent(typeof(PathCreator))]
public class PathManager : SingletonMonoBehaviour<PathManager>
{
    private List<APoint> _points;
    
    [ContextMenu("Generate")]
    private void Generate()
    {
        _points = new List<APoint>(GetComponentsInChildren<APoint>());
        if (_points.Count <= 0)
            return;
        
        Transform[] pointsTransforms = new Transform[_points.Count];
        for (int i = 0; i < pointsTransforms.Length; i++)
        {
            pointsTransforms[i] = _points[i].transform;
        }
            
        BezierPath bezierPath = new BezierPath (pointsTransforms);
        var pathCreator = GetComponent<PathCreator>();

        pathCreator.bezierPath = bezierPath;
        pathCreator.bezierPath.ControlPointMode = BezierPath.ControlMode.Free;
        pathCreator.bezierPath.FlipNormals = true;
    }

    protected override void Initialize()
    {
        _points = new List<APoint>(GetComponentsInChildren<APoint>());
        for (int i = 0; i < _points.Count; i++)
        {
            _points[i].Index = i;
        }
    }

    private void Start()
    {
        Debug.Log($"Points on path: {_points.Count}");
    }

    public APoint GetPoint(int index)
    {
        if (index >= _points.Count)
            return null;
        
        return _points[index];
    }
}
