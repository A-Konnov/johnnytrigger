﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointTrick : APoint
{
    protected override void ActivateVisit()
    {
        var trick = GetComponentInParent<ITrick>();
        if (trick != null)
        {
            if (!trick.IsTrickPlay)
            {
                var endPointTrick = (PointTrick) PathManager.GetPoint(Index + 1);
                trick.StartTrick(this, endPointTrick);
            }
            else
            {
                trick.EndTrick();
            }
        }
    }
}
