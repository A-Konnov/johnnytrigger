﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointFinish : APoint
{
    protected override void ActivateVisit()
    {
        CharacterProxy.Instance.CurSpeed = 0;
        CharacterProxy.Instance.OnFinish?.Invoke();
    }
}
