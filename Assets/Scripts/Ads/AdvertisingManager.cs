﻿
using UnityEngine;

public class AdvertisingManager : MonoBehaviour
{
    public static string USER_ID = "demoUserUnity";
    public static string KEY = "38760d6d" ;
    public static string REWARDED_INSTANCE_ID = "0";

    private void Start()
    {
        IronSourceConfig.Instance.setClientSideCallbacks (true);
        string id = IronSource.Agent.getAdvertiserId ();
        IronSource.Agent.validateIntegration();
        IronSource.Agent.init(KEY);
    }
}


