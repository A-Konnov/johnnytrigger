﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalProxy : Proxy<GlobalProxy, GlobalData>
{
    public TimeSpeedManager TimeSpeedManager { get; private set; }
    public ScoreManager ScoreManager { get; private set; }
    public AdvertisingManager AdvertisingManager  { get; private set; }
    public FacebookManager FacebookManager  { get; private set; }

    public override void GameAwake()
    {
        var managers = Object.FindObjectOfType<GameStart>().gameObject;
        TimeSpeedManager = managers.AddComponent<TimeSpeedManager>();
        ScoreManager = managers.AddComponent<ScoreManager>();
        AdvertisingManager = managers.AddComponent<AdvertisingManager>();
        FacebookManager = managers.AddComponent<FacebookManager>();
    }
}
