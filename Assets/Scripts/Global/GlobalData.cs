﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GlobalData", menuName = "GlobalData")]
public class GlobalData : ScriptableObject
{
    [SerializeField] private int _rewardForOneEnemy;

    public int RewardForOneEnemy => _rewardForOneEnemy;
}
