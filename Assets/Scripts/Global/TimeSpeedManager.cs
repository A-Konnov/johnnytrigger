﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeSpeedManager : MonoBehaviour
{
    public float SetTimeSpeed
    {
        set => Time.timeScale = value;
    }

    public void NormalSpeed()
    {
        Time.timeScale = 1;
    }
        
}
