﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class GameStart : SingletonMonoBehaviour<GameStart>
{
    private List<IEventsFunctions> _proxyList;
    
    private void Awake()
    {
        _proxyList = new List<IEventsFunctions>();
        
        _proxyList.Add(GlobalProxy.Instance);
        _proxyList.Add(UIProxy.Instance);
        _proxyList.Add(CharacterProxy.Instance);
        _proxyList.Add(BulletsProxy.Instance);

        foreach (var proxy in _proxyList)
        {
            proxy.GameAwake();
        }
    }

    private void Start()
    {
        foreach (var proxy in _proxyList)
        {
            proxy.GameStart();
        }
    }

    private void OnDestroy()
    {
        foreach (var proxy in _proxyList)
        {
            proxy.GameEnd();
        }
    }
}
