﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Enemy : MonoBehaviour
{
    private static readonly int ANIMATOR_ID_DYING = Animator.StringToHash("Dying");
    private Animator _animator;
    private ScoreManager _scoreManager;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _scoreManager = GlobalProxy.Instance.ScoreManager;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Bullet"))
            return;
        
        _animator.SetTrigger(ANIMATOR_ID_DYING);
        _scoreManager.AddScore();
    }
}
