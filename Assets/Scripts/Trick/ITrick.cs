﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITrick
{
    string Name { get; }
    float TimeSpeed { get; }
    bool IsTrickPlay { get; set; }
    float Distance { get; set; }
    void StartTrick(PointTrick startPointTrick, PointTrick endPointTrick);
    void EndTrick();
}
