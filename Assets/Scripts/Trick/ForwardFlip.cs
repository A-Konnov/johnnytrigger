﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForwardFlip : MonoBehaviour, ITrick
{
    [SerializeField] private float _timeSpeed = 0.5f;
    
    public string Name => TricksNotifications.ForwardFlip;

    public float TimeSpeed => _timeSpeed;
    public bool IsTrickPlay { get; set; }
    
    public float Distance { get; set; }

    public void StartTrick(PointTrick startPointTrick, PointTrick endPointTrick)
    {
        Distance = Math.Abs(startPointTrick.transform.position.x - endPointTrick.transform.position.x);
        CharacterProxy.Instance.CurTrick = this;
        GlobalProxy.Instance.TimeSpeedManager.SetTimeSpeed = _timeSpeed;
        Debug.Log($"Trick {Name} Start");
    }

    public void EndTrick()
    {
        CharacterProxy.Instance.CurTrick = null;
        GlobalProxy.Instance.TimeSpeedManager.NormalSpeed();
        Debug.Log($"Trick {Name} End");
    }
}
