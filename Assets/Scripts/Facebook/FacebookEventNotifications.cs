﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class FacebookEventNotifications
{
    public const string StartGame = "start_game";
    public const string FinishGame = "finish_game";
    public const string GetRewardedAds = "get_rewarded_ads";
}
