﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

public class FacebookManager : MonoBehaviour
{
    private delegate void FCEvent(string name, string message);
    
    private string Status { get; set; } = "Ready";
    private string LastResponse { get; set; } = string.Empty;

    private void Start()
    {
        FB.Init(OnInitComplete, OnHideUnity);
        Status = "FB.Init() called with " + FB.AppId;
        
        var fcEventDelegate = new FCEvent(EventInvoke);

        CharacterProxy.Instance.OnStart += () => fcEventDelegate(FacebookEventNotifications.StartGame, "Start Game");
        CharacterProxy.Instance.OnFinish += () => fcEventDelegate(FacebookEventNotifications.FinishGame, "Finish Game");
        UIProxy.Instance.FinishPanel.OnRewardX2  += () => fcEventDelegate(FacebookEventNotifications.GetRewardedAds, "Get Reward x2");
    }
    
    private void OnInitComplete()
    {
        Status = "Success - Check log for details";
        LastResponse = "Success Response: OnInitComplete Called\n";
        string logMessage = $"OnInitCompleteCalled IsLoggedIn='{FB.IsLoggedIn}' IsInitialized='{FB.IsInitialized}'";
        Debug.Log(logMessage);
        
        if (AccessToken.CurrentAccessToken != null)
        {
            Debug.Log(AccessToken.CurrentAccessToken.ToString());
        }
    }
    
    private void OnHideUnity(bool isGameShown)
    {
        Status = "Success - Check log for details";
        LastResponse = $"Success Response: OnHideUnity Called {isGameShown}\n";
        Debug.Log("Is game shown: " + isGameShown);
    }

    private void EventInvoke(string name, string message)
    {
        FB.LogAppEvent(name, null, new Dictionary<string, object>(){{name, message}});
        Debug.Log($"Facebook Event {name} send");
    }
}
