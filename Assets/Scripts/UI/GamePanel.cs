﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePanel : Panel
{
    private void Start()
    {
        CharacterProxy.Instance.OnStart += Open;
        CharacterProxy.Instance.OnFinish += Close;
    }
}
