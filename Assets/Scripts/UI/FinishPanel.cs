﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishPanel : Panel
{
    public Action OnRewardX2;
    
    private ScoreWidget _scoreWidget;

    public ScoreWidget ScoreWidget
    {
        get
        {
            if (_scoreWidget == null)
                _scoreWidget = GetComponentInChildren<ScoreWidget>();

            return _scoreWidget;
        }
    }
    
    
    private void Start()
    {
        CharacterProxy.Instance.OnFinish += Open;
    }
}
