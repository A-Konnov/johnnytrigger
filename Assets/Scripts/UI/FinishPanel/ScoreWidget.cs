﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreWidget : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _enemyCountTxt;
    [SerializeField] private TextMeshProUGUI _rewardCountTxt;
    
    private void Start()
    {
        CharacterProxy.Instance.OnFinish += Show;
    }

    private void Show()
    {
        var proxy = GlobalProxy.Instance;
        var score = proxy.ScoreManager.Score;
        var reward = score * proxy.Data.RewardForOneEnemy;

        _enemyCountTxt.text = $"x{score}";
        _rewardCountTxt.text = $"+{reward}";
    }

    public void ShowDouble()
    {
        var proxy = GlobalProxy.Instance;
        var score = proxy.ScoreManager.Score;
        var reward = score * proxy.Data.RewardForOneEnemy * 2;

        _enemyCountTxt.text = $"x{score}";
        _rewardCountTxt.text = $"+{reward}";
    }
}
