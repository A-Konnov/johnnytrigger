﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RewardBtns : MonoBehaviour
{
    [SerializeField] private Button _adsBtn;
    
    public void OnReward()
    {
        var curSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(curSceneIndex);
    }
    
    public void OnAdsReward()
    {
        if (IronSource.Agent.isRewardedVideoAvailable())
        {
            IronSource.Agent.showRewardedVideo();
        }
    }
    
    private void Start ()
    {	
        _adsBtn.interactable = false;

        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent; 
    }

    private void OnDestroy()
    {
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent -= RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent -= RewardedVideoAdRewardedEvent; 
    }
    
    void RewardedVideoAvailabilityChangedEvent (bool canShowAd)
    {
        _adsBtn.interactable = canShowAd;
    }
    
    void RewardedVideoAdRewardedEvent(IronSourcePlacement ssp)
    {
        _adsBtn.gameObject.SetActive(false);
        UIProxy.Instance.FinishPanel.ScoreWidget.ShowDouble();
        UIProxy.Instance.FinishPanel.OnRewardX2?.Invoke();
    }
}
