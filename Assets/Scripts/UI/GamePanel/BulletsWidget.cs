﻿
using UnityEngine;
using UnityEngine.UI;

public class BulletsWidget : MonoBehaviour
{
    private Image[] _images;
    private Color _colorAvailable;
    private Color _colorNotAvailable;

    private void Start()
    {
        Clear();
        Initialized();
    }

    private void Clear()
    {
        while (transform.childCount > 0)
        {
            DestroyImmediate(transform.GetChild(0).gameObject);
        }
    }

    private void Initialized()
    {
        var proxy = BulletsProxy.Instance;
        proxy.OnGetBullet += Refresh;
        
        var data = proxy.Data;
        
        var count = data.StartCount;
        var prefab = data.PrefabUI;
        _colorAvailable = data.ColorAvailable;
        _colorNotAvailable = data.ColorNotAvailable;
        _images = new Image[count];

        for (int i = 0; i < count; i++)
        {
            var image = Instantiate(prefab, transform).GetComponent<Image>();
            if (image)
            {
                image.color = _colorAvailable;
                _images[i] = image;
            }
        }
    }

    private void Refresh(int count)
    {
        for (int i = 0; i < _images.Length; i++)
        {
            _images[i].color = i < count ? _colorAvailable : _colorNotAvailable;
        }
    }
}
