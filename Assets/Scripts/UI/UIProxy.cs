﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;


public class UIProxy : Proxy<UIProxy>
{
    public StartPanel StartPanel { get; private set; }
    public FinishPanel FinishPanel { get; private set; }
    public GamePanel GamePanel  { get; private set; }

    public override void GameAwake()
    {
        StartPanel = Activate<StartPanel>();
        FinishPanel = Activate<FinishPanel>();
        GamePanel = Activate<GamePanel>();
    }

    private T Activate<T>() where T : MonoBehaviour
    {
        var findObjects = Object.FindObjectsOfTypeAll(typeof(T));
        if (findObjects.Length <= 0)
            return null;

        var monoBehaviour = (MonoBehaviour) findObjects[0];
        monoBehaviour.gameObject.SetActive(true);
        return (T) monoBehaviour;
    }
    
}
