﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPanel : Panel
{
    private void Start()
    {
        Open();
    }

    public void OnStart()
    {
        Close();
        CharacterProxy.Instance.OnStart?.Invoke();
    }
}
