﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class Singleton<T> where T : new()
{
    protected static T instance;

    public static T Instance
    {
        get
        {
            if (instance == null)
                instance = new T();

            return instance;
        }
    }
}


