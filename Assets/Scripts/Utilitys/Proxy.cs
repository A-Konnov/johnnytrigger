﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEventsFunctions
{
    void GameAwake();
    void GameStart();
    void GameEnd();
}

public class Proxy<TProxy, TData> : Proxy<TProxy> where TProxy : new() where TData : ScriptableObject
{
    private TData _data;
    public TData Data => _data;


    protected override void Initialize()
    {
        var typeName = typeof(TData).FullName;
        _data = Resources.Load(typeName) as TData;
    }
}


public class Proxy<TProxy> : IEventsFunctions
    where TProxy : new()
{
    protected static TProxy instance;

    public static TProxy Instance
    {
        get
        {
            if (instance == null)
                instance = new TProxy();

            return instance;
        }
    }

    protected Proxy()
    {
        Initialize();
    }
    
    protected virtual void Initialize(){}

    public virtual void GameAwake(){}

    public virtual void GameStart(){}

    public virtual void GameEnd(){}
}
