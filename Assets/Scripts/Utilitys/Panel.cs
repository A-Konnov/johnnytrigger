﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Panel : MonoBehaviour
{
    protected Animator animator;
    protected static readonly int ANIMATOR_ID = Animator.StringToHash("Open");
    protected virtual void Awake()
    {
        animator = GetComponent<Animator>();
    }

    protected void Open()
    {
        animator.SetBool(ANIMATOR_ID, true);
    }
    
    protected void Close()
    {
        animator.SetBool(ANIMATOR_ID, false);
    }
}
