﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = System.Object;

public class SingletonMonoBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T _instance;

    private void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = (T) FindObjectOfType(typeof(T));
            Initialize();
        }
    }

    protected virtual void Initialize(){}

    public static T Instance => _instance;
}
