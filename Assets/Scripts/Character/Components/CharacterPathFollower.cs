﻿using PathCreation;
using UnityEngine;

    public class CharacterPathFollower : MonoBehaviour
    {
        [SerializeField] private PathCreator _pathCreator;
        private float _speed = 0;
        private float _distanceTravelled;
        
        public float Speed
        {
            set => _speed = value;
        }

        private void Awake()
        {
            if (_pathCreator == null)
                _pathCreator = FindObjectOfType<PathCreator>();
        }

        private void Start()
        {
            if (_pathCreator != null)
            {
                _pathCreator.pathUpdated += OnPathChanged;
            }
        }

        private void Update()
        {
            if (_pathCreator != null)
            {
                _distanceTravelled += _speed * Time.deltaTime;
                transform.position = _pathCreator.path.GetPointAtDistance(_distanceTravelled);
            }
        }

        void OnPathChanged()
        {
            _distanceTravelled = _pathCreator.path.GetClosestDistanceAlongPath(transform.position);
        }
    }
