﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterShot : MonoBehaviour
{
    [SerializeField] private Transform _pointShot;
    private CharacterProxy _characterProxy;
    private BulletsProxy _bulletsProxy;
    
    public Transform PointShot => _pointShot;

    private void Awake()
    {
        _characterProxy = CharacterProxy.Instance;
        _bulletsProxy = BulletsProxy.Instance;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && _characterProxy.CurTrick != null)
        {
            var bullet = _bulletsProxy.GetBullet();
            
            if (!bullet) return;
            
            bullet.transform.position = _pointShot.position;
            bullet.Direction = _pointShot.right;
        }
    }
}