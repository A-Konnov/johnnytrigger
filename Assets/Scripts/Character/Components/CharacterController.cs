﻿
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    private CharacterPathFollower _characterPathFollower;
    private CharacterProxy _proxy;

    private float Speed
    {
        set
        {
            _characterPathFollower.Speed = value;
            _proxy.CurSpeed = value;
        }
    }

    private void Awake()
    {
        _characterPathFollower = GetComponent<CharacterPathFollower>();
        _proxy = CharacterProxy.Instance;
    }

    private void Start()
    {
        _proxy.OnStart += Run;
        _proxy.OnFinish += Stop;
    }

    private void Run()
    {
        Speed = _proxy.Data.StartSpeed;
    }

    private void Stop()
    {
        Speed = 0;
    }
}
