﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

[RequireComponent(typeof(CharacterShot))]
public class CharacterLaser : MonoBehaviour
{
    private CharacterProxy _characterProxy;
    private LineRenderer _lr;
    private bool _enable;
    private Transform _pointShot;
    private int _mask;
    

    private bool Enable
    {
        get => _enable;
        set
        {
            _enable = value;
            _lr.enabled = value;
        }
    }

    private void Awake()
    {
        _characterProxy = CharacterProxy.Instance;
        _pointShot = GetComponent<CharacterShot>().PointShot;
        _lr = GetComponent<LineRenderer>();
    }

    private void Start()
    {
        _mask = 1 << LayerMask.NameToLayer("Default");
        Enable = false;
    }

    private void LateUpdate()
    {
        if (_characterProxy.CurTrick != null)
        {
            if (!Enable)
                Enable = true;

            Draw();
        }
        else
        {
            if (Enable)
                Enable = false;
        }
    }

    private void Draw()
    {
        var startPoint = _pointShot.position;
        startPoint.z = 0;
        
        var direction = _pointShot.right;
        direction.z = 0;
        
        _lr.SetPosition(0, startPoint);

        if (Physics.Raycast(startPoint, direction, out var hit, 10, _mask))
        {
            if (hit.collider) _lr.SetPosition(1, hit.point);
        }
        else
        {
            _lr.SetPosition(1, direction * 100);
        }
    }
}