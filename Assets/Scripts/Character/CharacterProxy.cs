﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class CharacterProxy : Proxy<CharacterProxy, CharacterData>
{
    private CharacterController _characterController;

    public Action OnStart;
    public Action OnFinish;
    public float CurSpeed { get; set; }

    public CharacterController CharacterController
    {
        get
        {
            if (_characterController == null)
                _characterController = Object.FindObjectOfType<CharacterController>();

            return _characterController;
        }
    }
    
    public ITrick CurTrick { get; set; }

    public override void GameAwake()
    {
        _characterController = Object.FindObjectOfType<CharacterController>();
    }

    public override void GameEnd()
    {
        OnStart = null;
        OnFinish = null;
    }
}
