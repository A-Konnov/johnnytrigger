﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSMB : StateMachineBehaviour
{
    private CharacterProxy _proxy;
    
    private void OnEnable()
    {
        _proxy = CharacterProxy.Instance;
    }

    private void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        animator.SetBool("Run", _proxy.CurSpeed > 0f);

        var curTrick = _proxy.CurTrick; 
        
        if (curTrick != null && curTrick.IsTrickPlay == false)
        {
            animator.SetTrigger(curTrick.Name);
            animator.speed = 1f / curTrick.Distance;
            curTrick.IsTrickPlay = true;
        }
        
        if (curTrick == null)
        {
            animator.speed = 1f;
        }
    }
}
